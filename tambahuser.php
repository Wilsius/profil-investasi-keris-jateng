<?php
  include ('php/access.php');
  require_once ('php/connect.php');
        $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);
        if (mysqli_connect_errno()){
            die("Could not connect to database : ".myslqi_connect_error());
        }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Formulir Keris Jateng</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/2.ico" />

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">
    <link href="css/button.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/image.js"></script>
    <script type="text/javascript" src="js/image.js"></script>
    <script type="text/javascript" src="ckeditor3/ckeditor.js"></script>


  </head>

  <body id="page-top">


    <!-- Navigation -->
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a href="home.php" class="btn primary">
          <img src="img/21.jpg" class="pull-left"/>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
           <li class="nav-item active">
              <a class="nav-link" link href="lihatdatakabupaten.php">Daftar User</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" link href="lihatdatauser.php">Daftar Formulir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" link href="php/logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>


    <!-- Portfolio Grid -->

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="deskripsi">
        <div class="my-auto">
          <h1 class="mb-5"></h1><br/>
          <h1 class="mb-5"></h1><br/>
          <h2 class="mb-5">Data User</h2>
            <div class="container">
               <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                  <!-- Project Details Go Here -->
          <form method="post" action="php/saveKabbaru.php">

            <div class="form-group">
              <label for="usr">Kabupaten/ Kota :</label>
              <input type="text" name="kabkot" class="form-control" id="name" required><br/>
            </div>
            <div class="form-group">
              <label for="Lokasi">Username :</label>
              <textarea name="username" class="form-control" rows="5" cols="100" id="lokasi" required></textarea>
            </div>
            <div class="form-group">
              <label for="JSI">Password:</label><br/>
              <input type="text" class="form-control" name="password" required><br/>
            </div>
            <input type="submit" name="save" class="btn btn-info" value="Save" />
          </form><br/>

              </div>
            </div>
          </div>
        </div>
      </div>
      </section>

      <hr class="m-0">



    <!-- Footer -->
    <footer>
      <div class="container-fluid">
       <div class="p-3 mb-2 bg-dark text-white">
          <div class="row">
            <div class="col-md-4">
              <ul class="list-inline-quicklinks">
              </ul>
            </div>
            <div class="col-md-4">
              <ul class="list-inline quicklinks">
                <li class="list-inline-item">
                  <span class="copyright">Copyright © 2019 Keris Central Java Incorporated. All Rights Reserved</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>


  </body>

</html>



