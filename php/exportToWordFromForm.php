<?php  
 //export.php  
 if(isset($_POST["create_word"]))  
   {  
      include "docxtemplate.class.php";
      $docx = new DOCXTemplate('template.docx');

      //Deskripsi Proyek
      $namaProyek = $_POST['nama'];
      $lokasiProyek = $_POST['lokasi'];
      $jenisSektorInvestasi = $_POST['jsi'];
      $gambaranSingkat = $_POST['dskrps'];

      //Latar Belakang
      $maksudTujuan = $_POST['maksudtujuan'];
      $profilDaerah = $_POST['profil'];
      $perkembanganIndustri = $_POST['pid'];
      $potensiPeningkatan = $_POST['potensi'];
      $tingkatPartisipasi = $_POST['tingkatpartisipasi'];

      //Aspek Legalitas
      $pemenuhanKetentuan = $_POST['pkppub'];
      $perizinan = $_POST['akte'];
      $kajianResikoHukum = $_POST['kajian'];
      $penanggungJawab = $_POST['penanggungjawab'];

      //Aspek Pemasaran
      $analisisPermintaanPasar = $_POST['analisispasar'];
      $kajianSegmentasi = $_POST['kajiansegmen'];
      $kajianBauranPemasaran = $_POST['kajianpemasaran'];
      $estimasiPendapatanBisnis = $_POST['estimasipendapatan'];

      //Aspek Teknis
      $jaringanEnergiTenagaListrik = $_POST['jaringanenergi'];
      $jaringanTelekomunikasi = $_POST['jaringantelekomunikasi'];
      $jaringanSumberDaya = $_POST['jaringanair'];
      $sanitasi = $_POST['sanitasi'];
      $jaringanTransportasi = $_POST['jaringantransportasi'];
      $desainSpesifikasiTeknis = $_POST['desainteknis'];
      $biayaInvestasi = $_POST['estimasiinvestasi'];
      $biayaOperasional = $_POST['estimasioperasional'];
      $jadwalTahapanPelaksanaanKerja = $_POST['jadwalkerja'];

      //Aspek Manajemen dan Organisasi
      $polaManajemen = $_POST['polamanajemen'];
      $sistemPengelolaan = $_POST['sispengelolaan'];
      $kelembagaan = $_POST['kelembagaan'];

      //Aspek Sosial dan Lingkungan
      $identifikasiAwalDampakLingkungan = $_POST['identifikasilingkungan'];
      $rencanaPengadaanTanah = $_POST['rencanatanah'];
      $identifikasiAwalDampakSosial = $_POST['identifikasisosial'];
      $sumberDayaAlam = $_POST['sda'];
      $sumberDayaManusia = $_POST['sdm'];

      //Aspek Keuangan
      $analisisAlternatifPembiayaanBisnis = $_POST['analisisbisnis'];
      $penetapanFaktorDiskonto = $_POST['faktordiskonto'];
      $asumsiPenyusunanModelKeuangan = $_POST['asumsikeuangan'];
      $asumsiBelanjaModal = $_POST['asumsimodal'];
      $asumsiBiayaOperasionalPemeliharaan = $_POST['asumsioperasional'];
      $analisisPendapatanBisnis = $_POST['analisispendapatan'];
      $proyeksiKelayakanKeuangan = $_POST['hasilproyeksi'];
      $analisisSensivitas = $_POST['analisissensivitas'];

      //NaraHubung
      $namaInstansi = $_POST['nama_instansi'];
      $alamat = $_POST['alamat'];
      $noTelp = $_POST['notelp'];
      $email = $_POST['email'];

      //Lampiran Gambar
      //Lampiran Tabel

      //Deskripsi Proyek
      $docx->set('nama_proyek', $namaProyek);
      $docx->set('lokasi_proyek', $lokasiProyek);
      $docx->set('jenis_sektor_investasi', $jenisSektorInvestasi);
      $docx->set('gambaran_singkat', $gambaranSingkat);

      //Latar Belakang
      $docx->set('maksud_tujuan', $maksudTujuan);
      $docx->set('profil_daerah', $profilDaerah);
      $docx->set('perkembangan_industri', $perkembanganIndustri);
      $docx->set('potensi_peningkatan', $potensiPeningkatan);
      $docx->set('tingkat_partisipasi', $tingkatPartisipasi);

      //Aspek Legalitas
      $docx->set('pemenuhan_ketentuan', $pemenuhanKetentuan);
      $docx->set('perizinan',$perizinan);
      $docx->set('kajian_resiko_hukum',$kajianResikoHukum);
      $docx->set('penanggung_jawab',$penanggungJawab);

      //Aspek Pemasaran
      $docx->set('analisis_permintaan_pasar',$analisisPermintaanPasar);
      $docx->set('kajian_segmentasi',$kajianSegmentasi);
      $docx->set('kajian_bauran_pemasaran',$kajianBauranPemasaran);
      $docx->set('estimasi_pendapatan_bisnis',$estimasiPendapatanBisnis);

      //Aspek Teknis
      $docx->set('jaringan_energi_tenaga_listrik',$jaringanEnergiTenagaListrik);
      $docx->set('jaringan_telekomunikasi',$jaringanTelekomunikasi);
      $docx->set('jaringan_sumber_daya',$jaringanSumberDaya);
      $docx->set('sanitasi',$sanitasi);
      $docx->set('jaringan_transportasi',$jaringanTransportasi);
      $docx->set('desain_spesifikasi_teknis',$desainSpesifikasiTeknis);
      $docx->set('biaya_investasi',$biayaInvestasi);
      $docx->set('biaya_operasional',$biayaOperasional);
      $docx->set('jadwal_tahapan_pelaksanaan_kerja',$jadwalTahapanPelaksanaanKerja);

      //Aspek Manajemen dan Organisasi
      $docx->set('pola_manajemen',$polaManajemen);
      $docx->set('sistem_pengelolaan',$sistemPengelolaan);
      $docx->set('kelembagaan',$kelembagaan);

      //Aspek Sosial dan Lingkungan
      $docx->set('identifikasi_awal_dampak_lingkungan',$identifikasiAwalDampakLingkungan);
      $docx->set('rencana_pengadaan_tanah',$rencanaPengadaanTanah);
      $docx->set('identifikasi_awal_dampak_sosial',$identifikasiAwalDampakSosial);
      $docx->set('sumber_daya_alam',$sumberDayaAlam);
      $docx->set('sumber_daya_manusia',$sumberDayaManusia);

      //Aspek Keuangan
      $docx->set('analisis_alternatif_pembiayaan_bisnis',$analisisAlternatifPembiayaanBisnis);
      $docx->set('penetapan_faktor_diskonto',$penetapanFaktorDiskonto);
      $docx->set('asumsi_penyusunan_model_keuangan',$asumsiPenyusunanModelKeuangan);
      $docx->set('asumsi_belanja_modal',$asumsiBelanjaModal);
      $docx->set('asumsi_biaya_operasional_pemeliharaan',$asumsiBiayaOperasionalPemeliharaan);
      $docx->set('analisis_pendapatan_bisnis',$analisisPendapatanBisnis);
      $docx->set('proyeksi_kelayakan_keuangan',$proyeksiKelayakanKeuangan);
      $docx->set('analisis_sensivitas',$analisisSensivitas);

      //NaraHubung
      $docx->set('no_telp',$noTelp);
      $docx->set('email',$email);

      //Lampiran Gambar
      //Lampiran Tabel

      $docx->saveAs('test.docx');
      header("Content-Type:application/vnd.msword");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Disposition:attachment;filename=test.docx");
      readfile("test.docx");
   }  
 ?>  