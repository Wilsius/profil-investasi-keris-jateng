<?php session_start();
	require_once('connect.php');
	// Create connection
	$conn = mysqli_connect($db_host, $db_username, $db_password, $db_database);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . mysqli_connect_error());
	}

	$conn->close();
	session_destroy();
	echo '<script language="javascript">document.location="../index.php";</script>';
?>