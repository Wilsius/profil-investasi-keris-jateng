<?php  
 //Deskripsi Proyek

 require_once('access.php');
 require_once('connect.php');
 $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);  
 
if (isset($_POST['save'])){
      

      $analisisBisnis = filter_input(INPUT_POST,'analisisbisnis');
      $faktorDiskonto = filter_input(INPUT_POST,'faktordiskonto');
      $asumsiKeuangan = filter_input(INPUT_POST,'asumsikeuangan');
      $asumsiModal = filter_input(INPUT_POST,'asumsimodal');
      $asumsiOperasional = filter_input(INPUT_POST,'asumsioperasional');
      $analisisPendapatan = filter_input(INPUT_POST,'analisispendapatan');
      $hasilProyeksi = filter_input(INPUT_POST,'hasilproyeksi');
      $analisisSensivitas = filter_input(INPUT_POST,'analisissensivitas');
      $idAdmin = $_SESSION['id_admin'];
      $idDes = $_GET['id_deskripsi_proyek'];

      $query_save="INSERT INTO aspek_keuangan SET 
      analisis_alternatif_pembiayaan_bisnis='$analisisBisnis', 
      penetapan_faktor_diskonto='$faktorDiskonto', 
      asumsi_penyusunan_model_keuangan='$asumsiKeuangan',
      asumsi_belanja_modal='$asumsiModal',
      asumsi_biaya_operasional_pemeliharaan='$asumsiOperasional',
      analisis_pendapatan_bisnis='$analisisPendapatan',
      proyeksi_kelayakan_keuangan='$hasilProyeksi',
      analisis_sensivitas='$analisisSensivitas'
      WHERE id_deskripsi_proyek=$idDes
      ";

      $query_update="UPDATE aspek_keuangan SET 
      analisis_alternatif_pembiayaan_bisnis='$analisisBisnis', 
      penetapan_faktor_diskonto='$faktorDiskonto', 
      asumsi_penyusunan_model_keuangan='$asumsiKeuangan',
      asumsi_belanja_modal='$asumsiModal',
      asumsi_biaya_operasional_pemeliharaan='$asumsiOperasional',
      analisis_pendapatan_bisnis='$analisisPendapatan',
      proyeksi_kelayakan_keuangan='$hasilProyeksi',
      analisis_sensivitas='$analisisSensivitas' WHERE id_deskripsi_proyek=$idDes 
      ";
      if ($idDes==NULL){
         mysqli_query($conn,$query_save);
         echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../view.php?id_deskripsi_proyek='.$idDes.'";</script>';
      } else if($idDes){
         mysqli_query($conn,$query_update);
         echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../viewuser.php?id_deskripsi_proyek='.$idDes.'";</script>';  
      } else{
          echo '<script language="javascript">alert("Data Gagal Disimpan");document.location="../viewuser.php?id_deskripsi_proyek='.$idDes.'";</script>';
      }
      exit();
   }

 ?>  