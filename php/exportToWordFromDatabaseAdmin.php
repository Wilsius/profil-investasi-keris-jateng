<?php
require_once ('access.php');
require_once ('connect.php');
 //export.php  
 if(isset($_POST["create_word"]))  
   {  
      $idUser = $_SESSION['id_user'];

      $query="SELECT * FROM deskripsi_proyek
      JOIN latar_belakang
            ON deskripsi_proyek.iduser = latar_belakang.iduser
      JOIN aspek_legalitas
            ON deskripsi_proyek.iduser = aspek_legalitas.iduser
      JOIN aspek_pemasaran
            ON deskripsi_proyek.iduser = aspek_pemasaran.iduser
      JOIN aspek_teknis
            ON deskripsi_proyek.iduser = aspek_teknis.iduser
      JOIN aspek_manajemen_dan_organisasi
            ON deskripsi_proyek.iduser = aspek_manajemen_dan_organisasi.iduser
      JOIN aspek_sosial_dan_lingkungan
            ON deskripsi_proyek.iduser = aspek_sosial_dan_lingkungan.iduser
      JOIN aspek_keuangan
            ON deskripsi_proyek.iduser = aspek_keuangan.iduser
      JOIN nara_hubung
            ON deskripsi_proyek.iduser = nara_hubung.iduser
      WHERE deskripsi_proyek.iduser='$idUser'
      ";
      $result = mysqli_query($conn, $query);
      if(!$result){
      die("Gagal terhubung ke database : ".mysqli_error($conn));
      }
      $row = mysqli_fetch_assoc($result);
      
      include "docxtemplate.class.php";
      $docx = new DOCXTemplate('template.docx');

      //Deskripsi Proyek
      $docx->set('nama_proyek', $row['nama_proyek']);
      $docx->set('lokasi_proyek', $row['lokasi_proyek']);
      $docx->set('jenis_sektor_investasi', $row['jenis_sektor_investasi']);
      $docx->set('gambaran_singkat', $row['gambaran_singkat']);

      //Latar Belakang
      $docx->set('maksud_tujuan', $row['maksud_tujuan']);
      $docx->set('profil_daerah', $row['profil_daerah']);
      $docx->set('perkembangan_industri', $row['perkembangan_industri']);
      $docx->set('potensi_peningkatan', $row['potensi_peningkatan']);
      $docx->set('tingkat_partisipasi', $row['tingkat_partisipasi']);

      //Aspek Legalitas
      $docx->set('pemenuhan_ketentuan', $row['pemenuhan_ketentuan']);
      $docx->set('perizinan',$row['perizinan']);
      $docx->set('kajian_resiko_hukum',$row['kajian_resiko_hukum']);
      $docx->set('penanggung_jawab',$row['penanggung_jawab']);

      //Aspek Pemasaran
      $docx->set('analisis_permintaan_pasar', $row['analisis_permintaan_pasar']);
      $docx->set('kajian_segmentasi', $row['kajian_segmentasi']);
      $docx->set('kajian_bauran_pemasaran', $row['kajian_bauran_pemasaran']);
      $docx->set('estimasi_pendapatan_bisnis', $row['estimasi_pendapatan_bisnis']);

      //Aspek Teknis
      $docx->set('jaringan_energi_tenaga_listrik', $row['jaringan_energi_tenaga_listrik']);
      $docx->set('jaringan_telekomunikasi', $row['jaringan_telekomunikasi']);
      $docx->set('jaringan_sumber_daya', $row['jaringan_sumber_daya']);
      $docx->set('sanitasi', $row['sanitasi']);
      $docx->set('jaringan_transportasi', $row['jaringan_transportasi']);
      $docx->set('desain_spesifikasi_teknis', $row['desain_spesifikasi_teknis']);
      $docx->set('biaya_investasi', $row['biaya_investasi']);
      $docx->set('biaya_operasional', $row['biaya_operasional']);
      $docx->set('jadwal_tahapan_pelaksanaan_kerja', $row['jadwal_tahapan_pelaksanaan_kerja']);

      //Aspek Manajemen dan Organisasi
      $docx->set('pola_manajemen', $row['pola_manajemen']);
      $docx->set('sistem_pengelolaan', $row['sistem_pengelolaan']);
      $docx->set('kelembagaan', $row['kelembagaan']);

      //Aspek Sosial dan Lingkungan
      $docx->set('identifikasi_awal_dampak_lingkungan', $row['identifikasi_awal_dampak_lingkungan']);
      $docx->set('rencana_pengadaan_tanah', $row['rencana_pengadaan_tanah']);
      $docx->set('identifikasi_awal_dampak_sosial', $row['identifikasi_awal_dampak_sosial']);
      $docx->set('sumber_daya_alam', $row['sumber_daya_alam']);
      $docx->set('sumber_daya_manusia', $row['sumber_daya_manusia']);

      //Aspek Keuangan
      $docx->set('analisis_alternatif_pembiayaan_bisnis', $row['analisis_alternatif_pembiayaan_bisnis']);
      $docx->set('penetapan_faktor_diskonto', $row['penetapan_faktor_diskonto']);
      $docx->set('asumsi_penyusunan_model_keuangan', $row['asumsi_penyusunan_model_keuangan']);
      $docx->set('asumsi_belanja_modal', $row['asumsi_belanja_modal']);
      $docx->set('asumsi_biaya_operasional_pemeliharaan', $row['asumsi_biaya_operasional_pemeliharaan']);
      $docx->set('analisis_pendapatan_bisnis', $row['analisis_pendapatan_bisnis']);
      $docx->set('proyeksi_kelayakan_keuangan', $row['proyeksi_kelayakan_keuangan']);
      $docx->set('analisis_sensivitas', $row['analisis_sensivitas']);

      //NaraHubung
      $docx->set('nama_instansi', $row['nama_instansi']);
      $docx->set('alamat', $row['alamat']);
      $docx->set('no_telp', $row['no_telp']);
      $docx->set('email', $row['email']);


      $docx->saveAs('Profil Investasi.docx');
      header("Content-Type:application/vnd.msword");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Disposition:attachment;filename=Profil Investasi.docx");
      readfile("Profil Investasi.docx");
      exit();
   }  
?>