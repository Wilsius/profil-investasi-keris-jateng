<?php  
 //Deskripsi Proyek

 require_once('access.php');
 require_once('connect.php');
 $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);  
 
if (isset($_POST['save'])){
      

      $identifikasiLingkungan = filter_input(INPUT_POST,'identifikasilingkungan');
      $rencananTanah = filter_input(INPUT_POST,'rencanatanah');
      $identifikasiSosial = filter_input(INPUT_POST,'identifikasisosial');
      $SDA = filter_input(INPUT_POST,'sda');
      $SDM = filter_input(INPUT_POST,'sdm');
      $idAdmin = $_SESSION['id_admin'];
      $idDes = $_GET['id_deskripsi_proyek'];

      $query_save="INSERT INTO aspek_sosial_dan_lingkungan SET 
      identifikasi_awal_dampak_lingkungan='$identifikasiLingkungan', 
      rencana_pengadaan_tanah='$rencananTanah', 
      identifikasi_awal_dampak_sosial='$identifikasiSosial',
      sumber_daya_alam='$SDA',
      sumber_daya_manusia='$SDM'
      WHERE id_deskripsi_proyek=$idDes
      ";

      $query_update="UPDATE aspek_sosial_dan_lingkungan SET 
      identifikasi_awal_dampak_lingkungan='$identifikasiLingkungan', 
      rencana_pengadaan_tanah='$rencananTanah', 
      identifikasi_awal_dampak_sosial='$identifikasiSosial',
      sumber_daya_alam='$SDA',
      sumber_daya_manusia='$SDM' WHERE id_deskripsi_proyek=$idDes 
      ";
      if ($idDes==NULL){
         mysqli_query($conn,$query_save);
         echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../view.php?id_deskripsi_proyek='.$idDes.'";</script>';
      } else if($idDes){
         mysqli_query($conn,$query_update);
         echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../viewuser.php?id_deskripsi_proyek='.$idDes.'";</script>';  
      } else{
          echo '<script language="javascript">alert("Data Gagal Disimpan");document.location="../viewuser.php?id_deskripsi_proyek='.$idDes.'";</script>';
      }
      exit();
   }

 ?>  