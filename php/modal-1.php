<?php  
 //Deskripsi Proyek

 require_once('access.php');
 require_once('connect.php');
 $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);  
 
if (isset($_POST['save'])){
      $namaProyek = filter_input(INPUT_POST,'nama');
      $lokasiProyek = filter_input(INPUT_POST,'lokasi');
      $jenisSektorInvestasi = filter_input(INPUT_POST,'jsi');
      $gambaranSingkat = filter_input(INPUT_POST, 'dskrps');
      $idUser = $_SESSION['id_user'];

      $query_desMax  = "SELECT MAX(id_deskripsi_proyek) as Max FROM deskripsi_proyek";

      $max = mysqli_query($conn, $query_desMax);
      $desMax = mysqli_fetch_array($max);

      $idDes = $desMax['Max']+1;


      $query="INSERT INTO deskripsi_proyek SET id_deskripsi_proyek='$idDes',
      iduser='$idUser', 
      nama_proyek='$namaProyek', 
      lokasi_proyek='$lokasiProyek', 
      jenis_sektor_investasi='$jenisSektorInvestasi', 
      gambaran_singkat='$gambaranSingkat' 
      ";

      $query_latar_create = "INSERT INTO latar_belakang SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_legalitas_create = "INSERT INTO aspek_legalitas SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_pemasaran_create = "INSERT INTO aspek_pemasaran SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_teknis_create = "INSERT INTO aspek_teknis SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_manajemen_create = "INSERT INTO aspek_manajemen_dan_organisasi SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_sosial_create = "INSERT INTO aspek_sosial_dan_lingkungan SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_keuangan_create = "INSERT INTO aspek_keuangan SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";
      $query_narahubung_create = "INSERT INTO nara_hubung SET id_deskripsi_proyek = '$idDes', iduser='$idUser'";

      if( mysqli_query($conn,$query) ) {
            mysqli_query($conn,$query_latar_create);
            mysqli_query($conn,$query_legalitas_create);
            mysqli_query($conn,$query_pemasaran_create);
            mysqli_query($conn,$query_teknis_create);
            mysqli_query($conn,$query_manajemen_create);
            mysqli_query($conn,$query_sosial_create);
            mysqli_query($conn,$query_keuangan_create);
            mysqli_query($conn,$query_narahubung_create);

            echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../home.php?id_deskripsi_proyek='.$idDes.'#latar";</script>';
      }else{
          echo '<script language="javascript">alert("Data Gagal Disimpan");document.location="../home.php";</script>';
      }

   }elseif (isset($_POST['load_data'])){
      $idUser = $_SESSION['id_user'];

      $query="SELECT * FROM deskripsi_proyek WHERE iduser='$idUser' ";
      $result = mysqli_query($conn, $query);
      $row = mysqli_fetch_assoc($result);
      
      $namaProyek = $row['nama_proyek'];
      $lokasiProyek = $row['lokasi_proyek'];
      $jenisSektorInvestasi = $row['jenis_sektor_investasi'];
      $gambaranSingkat = $row['gambaran_singkat'];
   }
 ?>  