<?php
	include('access.php');
	include ('connect.php');
	$db= mysqli_connect($db_host,$db_username,$db_password,$db_database);

	$idUser = $_SESSION['id_user'];

	$query="SELECT * FROM deskripsi_proyek 
  JOIN latar_belakang
    ON deskripsi_proyek.iduser = latar_belakang.iduser
  JOIN aspek_legalitas
    ON deskripsi_proyek.iduser = aspek_legalitas.iduser
  JOIN aspek_pemasaran
    ON deskripsi_proyek.iduser = aspek_pemasaran.iduser
  JOIN aspek_teknis
    ON deskripsi_proyek.iduser = aspek_teknis.iduser
  JOIN aspek_manajemen_dan_organisasi
    ON deskripsi_proyek.iduser = aspek_manajemen_dan_organisasi.iduser
  JOIN aspek_sosial_dan_lingkungan
    ON deskripsi_proyek.iduser = aspek_sosial_dan_lingkungan.iduser
  JOIN aspek_keuangan
    ON deskripsi_proyek.iduser = aspek_keuangan.iduser
  JOIN nara_hubung
    ON deskripsi_proyek.iduser = nara_hubung.iduser
  WHERE iduser='$idUser'
  ";
  $result = mysqli_query($conn, $query);
  $row = mysqli_fetch_assoc($result);
  
  $namaProyek = $row['nama_proyek'];
  $lokasiProyek = $row['lokasi_proyek'];
  $jenisSektorInvestasi = $row['jenis_sektor_investasi'];
  $gambaranSingkat = $row['gambaran_singkat'];

  $maksudTujuan = $row['maksud_tujuan'];
  $profilDaerah = $row['profil_daerah'];
  $perkembanganIndustri = $row['perkembangan_industri'];
  $potensiPeningkatan = $row['potensi_peningkatan'];
  $tingkatPartisipasi = $row['tingkat_partisipasi'];

  $pemenuhanKetentuan = $row['pemenuhan_ketentuan'];
  $perizinan = $row['perizinan'];
  $kajianResikoHukum = $row['kajian_resiko_hukum'];
  $penanggungJawab = $row['penanggung_jawab'];

  $analisisPermintaanPasar = $row['analisis_permintaan_pasar'];
  $kajianSegmentasi = $row['kajian_segmentasi'];
  $kajianBauranPemasaran = $row['kajian_bauran_pemasaran'];
  $estimasiPendapatanBisnis = $row['estimasi_pendapatan_bisnis'];

  $jaringanEnergiTenagaListrik = $row['jaringan_energi_tenaga_listrik'];
  $jaringanTelekomunikasi = $row['jaringan_telekomunikasi'];
  $jaringanSumberDaya = $row['jaringan_sumber_daya'];
  $sanitasi = $row['sanitasi'];
  $jaringanTransportasi = $row['jaringan_transportasi'];
  $desainSpesifikasiTeknis = $row['desain_spesifikasi_teknis'];
  $biayaInvestasi = $row['biaya_investasi'];
  $biayaOperasional = $row['biaya_operasional'];
  $jadwalTahapanPelaksanaanKerja = $row['jadwal_tahapan_pelaksanaan_kerja'];

  $polaManajemen = $row['pola_manajemen'];
  $sistemPengelolaan = $row['sistem_pengelolaan'];
  $kelembagaan = $row['kelembagaan'];

  $identifikasiAwalDampakLingkungan = $row['identifikasi_awal_dampak_lingkungan'];
  $rencanaPengadaanTanah = $row['rencana_pengadaan_tanah'];
  $identifikasiAwalDampakSosial = $row['identifikasi_awal_dampak_sosial'];
  $sumberDayaAlam = $row['sumber_daya_alam'];
  $sumberDayaManusia = $row['sumber_daya_manusia'];

  $analisisAlternatifPembiayaanBisnis = $row['analisis_alternatif_pembiayaan_bisnis'];
  $penetapanFaktorDiskonto = $row['penetapan_faktor_diskonto'];
  $asumsiPenyusunanModelKeuangan = $row['asumsi_penyusunan_model_keuangan'];
  $asumsiBelanjaModal = $row['asumsi_belanja_modal'];
  $asumsiBiayaOperasionalPemeliharaan = $row['asumsi_biaya_operasional_pemeliharaan'];
  $analisisPendapatanBisnis = $row['analisis_pendapatan_bisnis'];
  $proyeksiKelayakanKeuangan = $row['proyeksi_kelayakan_keuangan'];
  $analisisSensivitas = $row['analisis_sensivitas'];

  $namaInstansi = $row['nama_instansi'];
  $alamat = $row['alamat']; 
  $noTelp = $row['no_telp'];
  $email = $row['email'];
?>