<?php  
 //Deskripsi Proyek

 require_once('access.php');
 require_once('connect.php');
 $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);  
 
if (isset($_POST['save'])){
      

     $jaringanEnergi = filter_input(INPUT_POST,'jaringanenergi');
      $jaringanTelekomunikasi = filter_input(INPUT_POST,'jaringantelekomunikasi');
      $jaringanAir = filter_input(INPUT_POST,'jaringanair');
      $Sanitasi = filter_input(INPUT_POST,'sanitasi');
      $jaringanTransportasi = filter_input(INPUT_POST,'jaringantransportasi');
      $desainTeknis = filter_input(INPUT_POST,'desainteknis');
      $estimasiInvestasi = filter_input(INPUT_POST,'estimasiinvestasi');
      $estimasiOperasional = filter_input(INPUT_POST,'estimasioperasional');
      $jadwalKerja = filter_input(INPUT_POST,'jadwalkerja');
      $idAdmin = $_SESSION['id_admin'];
      $idDes = $_GET['id_deskripsi_proyek'];

      $query_save="INSERT INTO aspek_teknis SET 
      jaringan_energi_tenaga_listrik='$jaringanEnergi', 
      jaringan_telekomunikasi='$jaringanTelekomunikasi', 
      jaringan_sumber_daya='$jaringanAir',
      sanitasi='$Sanitasi', 
      jaringan_transportasi='$jaringanTransportasi',
      desain_spesifikasi_teknis='$desainTeknis',
      biaya_investasi='$estimasiInvestasi',
      biaya_operasional='$estimasiOperasional',
      jadwal_tahapan_pelaksanaan_kerja='$jadwalKerja' WHERE id_deskripsi_proyek=$idDes
      ";

      $query_update="UPDATE aspek_teknis SET 
      jaringan_energi_tenaga_listrik='$jaringanEnergi', 
      jaringan_telekomunikasi='$jaringanTelekomunikasi', 
      jaringan_sumber_daya='$jaringanAir',
      sanitasi='$Sanitasi', 
      jaringan_transportasi='$jaringanTransportasi',
      desain_spesifikasi_teknis='$desainTeknis',
      biaya_investasi='$estimasiInvestasi',
      biaya_operasional='$estimasiOperasional',
      jadwal_tahapan_pelaksanaan_kerja='$jadwalKerja' WHERE id_deskripsi_proyek=$idDes 
      ";
      if ($idDes==NULL){
         mysqli_query($conn,$query_save);
         echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../view.php?id_deskripsi_proyek='.$idDes.'";</script>';
      } else if($idDes){
         mysqli_query($conn,$query_update);
         echo '<script language="javascript">alert("Data Berhasil Disimpan");document.location="../viewuser.php?id_deskripsi_proyek='.$idDes.'";</script>';  
      } else{
          echo '<script language="javascript">alert("Data Gagal Disimpan");document.location="../viewuser.php?id_deskripsi_proyek='.$idDes.'";</script>';
      }
      exit();
   }

 ?>  