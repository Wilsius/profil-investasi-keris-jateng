<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Halaman Login</title>
	<!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/2.ico" />

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
	  <link href="css/button.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>
<body id = "page-top">
	 <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Selamat Datang di Keris Jateng</div>
          <div class="intro-heading text-uppercase">Formulir Profil Investasi Proyek</div>
          <form action="php/login.php" method="post">
		          		<h3 class="section-subheading text-muted"></h3>

						<div class="form-group">
							<label for="username"><b>Username   :   </b></label>
					    	<input type="text" placeholder="Masukkan Username"  name="username" pattern="[a-z0-9_]{6,15}"
              title="Username hanya menggunakan huruf kecil dan garis bawah"
              required><br/>
					   	</div>

					    <div class="form-group">
						    <label for="password"><b>Password    :   </b></label>
						    <input type="password" placeholder="Masukkan Password" name="password" pattern="[a-z0-9_]{6,15}"
              title="Password hanya menggunakan huruf kecil dan garis bawah"><br/>
						</div>

					    <input type="submit" class="btn btn-dark" name="login" value="Login">
			</form>
        </div>
      </div>
    </header>
</body>
</html>