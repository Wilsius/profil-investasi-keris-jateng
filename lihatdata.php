<?php
  require_once ('php/access.php');
  require_once ('php/connect.php');
        $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);
        if (mysqli_connect_errno()){
            die("Could not connect to database : ".myslqi_connect_error());
        } 
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Lihat Data</title>
	<!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/2.ico" />

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
	  <link href="css/button.css" rel="stylesheet">
    <link href="css/table.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/image.js"></script>
</head>
<body id = "page-top">
	 <!-- Header -->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
      <div class="container">
        <a href="home.php" class="btn primary">
          <img src="img/21.jpg" class="pull-left"/>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link" link href="home.php"> Buat Formulir</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" link href="#">Daftar Formulir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" link href="php/logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div id="content">
            <!--Tempat Load Data-->

    </div><br/>
    <h1 class="mb-5"></h1><br/>
    <table class="table table-bordered table-dark">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Nama Proyek</th>
          <th scope="col">Kabupaten</th>
          <th scope="col">Jenis</th>
          <th scope="col">Opsi</th>
        </tr>
      </thead>
      <tbody>
      <?php
      include ('php/connect.php');
      $idUser = $_SESSION['id_user'];
      $namaKabKota = $_SESSION['nama_kota_kabupaten'];
      $query="SELECT id_deskripsi_proyek, nama_proyek, lokasi_proyek, jenis_sektor_investasi FROM deskripsi_proyek WHERE iduser='$idUser'";
      $result = $conn-> query($query);
        if ($result->num_rows > 0){
          while ($row = $result-> fetch_assoc()){
          echo "<tr>";
          echo "<td>".$row["id_deskripsi_proyek"]."</td>";
          echo "<td>".$row["nama_proyek"]."</td>";
          echo "<td>".$namaKabKota."</td>";
          echo "<td>".$row["jenis_sektor_investasi"]."</td>";
          echo "<td><a href='view.php?id_deskripsi_proyek=".$row['id_deskripsi_proyek']."'>View & Edit</a></td>";
          echo "<td><a href='php/delete-1.php?id_deskripsi_proyek=".$row['id_deskripsi_proyek']."'>Delete</a></td>";
          echo "</tr>";
          }
          echo "</tbody>";
          echo "</table>";
        }
        else {
          echo "0 result";
        }

        $conn->close();

      ?>

   <!--  <script type="text/javascript">
                  $(document).ready(function(){
                    loadData();

                    $('form').on('save_data', function(e){
                      e.preventDefault();
                      $.ajax({
                        type:$(this).attr('method'),
                        url:$(this).attr('action'),
                        data:$(this).serialize(),
                        success:function(){
                          loadData();
                          resetForm();
                        }
                      });
                    })
                  })

                  function loadData(){
                    $.get('php/data-1.php', function(data){
                      $('#content').html(data);
                      $('.hapusData').click(function(e){
                        e.preventDefault();
                          $.ajax({
                          type:'get',
                          url:$(this).attr('href'),
                          success:function(){
                            loadData();
                          }
                        });
                      })
                    })
                  }

                  function resetForm(){
                    $('[type=text]').val('');
                    $('[name=nama]').focus();
                  }

          </script> -->
</body>
</html>