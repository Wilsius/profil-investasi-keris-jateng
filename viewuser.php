<?php
  require_once ('php/access.php');
  require_once ('php/connect.php');
        $db= mysqli_connect($db_host,$db_username,$db_password,$db_database);
        if (mysqli_connect_errno()){
            die("Could not connect to database : ".myslqi_connect_error());
        }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Formulir Keris Jateng</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/2.ico" />

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">
    <link href="css/button.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/image.js"></script>
    <script type="text/javascript" src="js/image.js"></script>
    <script type="text/javascript" src="ckeditor3/ckeditor.js"></script>


  </head>

  <body id="page-top">


    <!-- Navigation -->
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a href="home.php" class="btn primary">
          <img src="img/21.jpg" class="pull-left"/>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Isi Formulir
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#deskripsi">Deskripsi Proyek</a>
                <a class="dropdown-item" href="#latar">Latar Belakang</a>
                <a class="dropdown-item" href="#legalitas">Aspek Legalitas</a>
                <a class="dropdown-item" href="#pasar">Aspek Pemasaran</a>
                <a class="dropdown-item" href="#teknis">Aspek Teknis</a>
                <a class="dropdown-item" href="#manajemen">Aspek Manajemen</a>
                <a class="dropdown-item" href="#sosial">Aspek Sosial dan Lingkungan</a>
                <a class="dropdown-item" href="#keuangan">Aspek Keuangan</a>
                <a class="dropdown-item" href="#narahubung">Narahubung</a>
              </div>
            </li>
            <li class="nav-item active">
              <a class="nav-link" link href="lihatdatakabupaten.php">Daftar User</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" link href="lihatdatauser.php">Daftar Formulir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" link href="php/logout.php">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>


    <!-- Portfolio Grid -->
    <?php
      $idDes = $_GET['id_deskripsi_proyek'];
      $query="SELECT nama_proyek, lokasi_proyek, jenis_sektor_investasi, gambaran_singkat FROM deskripsi_proyek WHERE id_deskripsi_proyek='$idDes'";
      $result = $conn-> query($query);
      if ($result->num_rows > 0){
        while ($row = $result-> fetch_assoc()){
          $namaProyek = $row['nama_proyek'];
          $lokasiProyek = $row['lokasi_proyek'];
          $jenisSektorInvestasi = $row['jenis_sektor_investasi'];
          $gambaranSingkat = $row['gambaran_singkat'];
        }
      }
    ?>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="deskripsi">
        <div class="my-auto">
          <h1 class="mb-5"></h1><br/>
          <h1 class="mb-5"></h1><br/>
          <h2 class="mb-5">Deskripsi Proyek</h2>
            <div class="container">
               <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                  <!-- Project Details Go Here -->
          <form method="post" action="php/saveadmin1.php?id_deskripsi_proyek=<?php echo($idDes)?>">

            <div class="form-group">
              <label for="usr">Nama Proyek :</label>
              <input type="text" name="nama" class="form-control" id="name" value= "<?php echo $namaProyek ?>" required><br/>
            </div>
            <div class="form-group">
              <label for="Lokasi">Lokasi Proyek :</label>
              <textarea name="lokasi" class="form-control" rows="5" cols="100" id="lokasi" required><?php echo $lokasiProyek ?></textarea>
            </div>
            <div class="form-group">
              <label for="JSI">Jenis Sektor Investasi:</label><br/>
              <label>(contoh: Infrastruktur, perdagangan, atau pariwisata)</label>
              <input type="text" class="form-control" name="jsi" value= "<?php echo $jenisSektorInvestasi ?>"required><br/>
            </div>
            <div class="form-group">
              <label for="comment">Gambaran singkat mengenai proyek :</label><br/>
              <label> (Deskripsi Proyek) :</label>  
              <textarea class="form-control" rows="5" cols="100" name="dskrps" required><?php echo $gambaranSingkat ?></textarea><br/>
            </div>
            <input type="submit" name="save" class="btn btn-info" value="Save" />
          </form><br/>

              </div>
            </div>
          </div>
        </div>
      </div>
      </section>

      <hr class="m-0">

      <?php
        $idUser = $_SESSION['id_admin'];
        $idDes = $_GET['id_deskripsi_proyek'];
        $query="SELECT maksud_tujuan, profil_daerah, perkembangan_industri, potensi_peningkatan, tingkat_partisipasi FROM latar_belakang WHERE id_deskripsi_proyek='$idDes'";
        $result = $conn-> query($query);
        if ($result->num_rows > 0){
          while ($row = $result-> fetch_assoc()){
            $maksudTujuan = $row['maksud_tujuan'];
            $profilDaerah = $row['profil_daerah'];
            $perkembanganIndustri = $row['perkembangan_industri'];
            $potensiPeningkatan = $row['potensi_peningkatan'];
            $tingkatPartisipasi = $row['tingkat_partisipasi'];
          }
        } else {
            $maksudTujuan = '';
            $profilDaerah = '';
            $perkembanganIndustri = '';
            $potensiPeningkatan = '';
            $tingkatPartisipasi = '';
        }
      ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="latar">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Latar Belakang</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin2.php?id_deskripsi_proyek=<?php echo($idDes)?>" enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="maksud">Maksud dan Tujuan Pengembangan :</label><br/>
                        <label>(tujuan dari saudara mengembangkan proyek tersebut)</label>
                        <textarea name="maksudtujuan" class="form-control" rows="5" cols="100" ><?php echo $maksudTujuan; ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="Profil">Profil Daerah :</label>
                        <textarea name="profil" class="form-control" rows="5" cols="100" ><?php echo $profilDaerah; ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="PID">Perkembangan Industri di Daerah :</label>
                        <textarea name="pid" class="form-control" rows="5" cols="100" ><?php echo $perkembanganIndustri; ?></textarea>
                      </div>
                      <div class="form-group">
                        <label>Gambaran singkat tentang alasan kenapa dikategorikan sebagai prioritas investasi.</label><br/>
                        <label>a. Potensi peningkatan pendapatan masyarakat</label>
                        <label>(termasuk menghitung dampak tidak langsung dari keberadaan proyek) :</label>
                        <textarea class="form-control" name="potensi" rows="5" cols="100" ><?php echo $potensiPeningkatan; ?></textarea><br/>
                        <label>b. Tingkat partisipasi masyarakat terhadap proyek investasi tersebut :</label>
                        <label>(berpartisipasi, bermitra, aktor utama seperti pengelolaan pariwisata di Bali) :</label>
                        <textarea class="form-control" name="tingkatpartisipasi" rows="5" cols="100" ><?php echo $tingkatPartisipasi; ?></textarea><br/>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">
        
        <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT pemenuhan_ketentuan, perizinan, kajian_resiko_hukum, penanggung_jawab FROM aspek_legalitas WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $pemenuhanKetentuan = $row['pemenuhan_ketentuan'];
              $Perizinan = $row['perizinan'];
              $kajianResiko = $row['kajian_resiko_hukum'];
              $penanggungJawab = $row['penanggung_jawab'];
            }
          }else {
            $pemenuhanKetentuan = '';
            $Perizinan = '';
            $kajianResiko = '';
            $penanggungJawab = '';
        }
        ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="legalitas">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Aspek Legalitas</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin3.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                     <div class="form-group">
                        <label for="PKPPU-B">Pemenuhan Ketentuan Peraturan Perundang-Undangan Bisnis :</label>
                        <textarea class="form-control" rows="5" cols="100" name="pkppub" required><?php echo $pemenuhanKetentuan ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="Akte">Akte–akte dan perizinan yang diperlukan :</label>
                        <!-- <textarea class="ckeditor" id="ckeditor" name="akte" rows="5" cols="70" ><?php echo $Perizinan ?></textarea> -->
                        <input type="file" class="form-control" name="akte" multiple /><?php echo $Perizinan ?>
                        <br>

                      </div>
                      <div class="form-group">
                        <label for="Kajian">Kajian Risiko Hukum, Peraturan, Perijinan dan cara mitigasinya/menanganinya :</label>
                        <textarea class="form-control" name="kajian" rows="5" cols="100" required><?php echo $kajianResiko ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="Penanggungjawab">Penanggung jawab proyek (PIC yang mengerti mengenai seluk-beluk proyek tersebut) :</label>
                        <textarea class="form-control" name="penanggungjawab" rows="5" cols="100" required><?php echo $penanggungJawab ?></textarea>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" onclick="toastr.success('Hi! I am success message.');" value="Save" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">

        <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT analisis_permintaan_pasar, kajian_segmentasi, kajian_bauran_pemasaran, estimasi_pendapatan_bisnis FROM aspek_pemasaran WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $analisisPasar = $row['analisis_permintaan_pasar'];
              $kajianSegmentasi = $row['kajian_segmentasi'];
              $kajianBauran = $row['kajian_bauran_pemasaran'];
              $estimasiPendapatan = $row['estimasi_pendapatan_bisnis'];
            }
          }else {
            $analisisPasar = '';
            $kajianSegmentasi = '';
            $kajianBauran = '';
            $estimasiPendapatan = '';
        }
        ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="pasar">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Aspek Pemasaran</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin4.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                     <div class="form-group">
                        <label for="AnalisisPasar">Analisis Permintaan Pasar (Demand) :</label>
                        <textarea class="form-control" name="analisispasar" rows="5" cols="100" required><?php echo $analisisPasar ?></textarea>
                      </div>
                      <div class="form-group">
                        <label>Analisis Pemasaran</label><br/>
                        <label>a. Kajian Segmentasi (Targeting dan Positioning) :</label>
                        <textarea class="form-control" name="kajiansegmen" rows="5" cols="100" required><?php echo $kajianSegmentasi ?></textarea>
                        <label>b. Kajian Bauran Pemasaran (Titik beratnya dalam penentuan harga) :</label>
                        <textarea class="form-control" name="kajianpemasaran" rows="5" cols="100" required><?php echo $kajianBauran ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="EstimasiPendapatan">Estimasi Pendapatan Bisnis :</label>
                        <textarea class="form-control" name="estimasipendapatan" rows="5" cols="100" required><?php echo $estimasiPendapatan ?></textarea>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">

        <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT jaringan_energi_tenaga_listrik, jaringan_telekomunikasi, jaringan_sumber_daya, sanitasi, jaringan_transportasi, desain_spesifikasi_teknis, biaya_investasi, biaya_operasional, jadwal_tahapan_pelaksanaan_kerja FROM aspek_teknis WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $jaringanEnergi = $row['jaringan_energi_tenaga_listrik'];
              $jaringanTelekomunikasi = $row['jaringan_telekomunikasi'];
              $jaringanSumberdaya = $row['jaringan_sumber_daya'];
              $Sanitasi = $row['sanitasi'];
              $jaringanTransportasi = $row['jaringan_transportasi'];
              $desainSpesifikasi = $row['desain_spesifikasi_teknis'];
              $biayaInvestasi = $row['biaya_investasi'];
              $biayaOperasional = $row['biaya_operasional'];
              $jadwalTahapan = $row['jadwal_tahapan_pelaksanaan_kerja'];
            }
          }else {
            $jaringanEnergi = '';
            $jaringanTelekomunikasi = '';
            $jaringanSumberdaya = '';
            $Sanitasi = '';
            $jaringanTransportasi = '';
            $desainSpesifikasi = '';
            $biayaInvestasi = '';
            $biayaOperasional = '';
            $jadwalTahapan = '';
        }
        ?>


        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="teknis">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Aspek Teknis</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin5.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                     <div class="form-group">
                        <label>Kebutuhan Infrastruktur dan Sarana Pendukung</label><br/>
                        <label for="JaringanEnergi">a. Jaringan Energi dan Tenaga Listrik :</label>
                        <textarea class="form-control" name="jaringanenergi" rows="5" cols="100" required><?php echo $jaringanEnergi ?></textarea>
                        <label for="JaringanTelekomunikasi">b. Jaringan Telekomunikasi :</label>
                        <textarea class="form-control" name="jaringantelekomunikasi" rows="5" cols="100" required><?php echo $jaringanTelekomunikasi ?></textarea>
                        <label for="JaringanAir">c. Jaringan Sumber Daya Air dan Jaminan Pasokan Air :</label>
                        <textarea class="form-control" name="jaringanair" rows="5" cols="100" required><?php echo $jaringanSumberdaya ?></textarea>
                        <label for="Sanitasi">d. Sanitasi :</label>
                        <textarea class="form-control" name="sanitasi" rows="5" cols="100" required><?php echo $Sanitasi ?></textarea>
                        <label for="JaringanTransportasi">e. Jaringan Transportasi :</label>
                        <textarea class="form-control" name="jaringantransportasi" rows="5" cols="100" required><?php echo $jaringanTransportasi ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="DesainTeknis">Desain dan Spesifikasi Teknis (Bangunan, Mesin, dan Lain-lain) :</label>
                        <textarea class="form-control" name="desainteknis" rows="5" cols="100" required><?php echo $desainSpesifikasi ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="EstimasiInvestasi">Estimasi Biaya Investasi (Bangunan dan Mesin-mesin) :</label>
                        <textarea class="form-control" name="estimasiinvestasi" rows="5" cols="100" required><?php echo $biayaInvestasi ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="EstimasiOperasional">Estimasi Biaya Operasional (Persediaan bahan baku, bahan pendukung) :</label>
                        <textarea class="form-control" name="estimasioperasional" rows="5" cols="100" required><?php echo $biayaOperasional ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="JadwalKerja">Jadwal Pelaksanaan Kerja dan Tahapan Pelaksanaan Kerja :</label>
                        <textarea class="form-control" name="jadwalkerja" rows="5" cols="100" required><?php echo $jadwalTahapan ?></textarea>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">

         <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT pola_manajemen, sistem_pengelolaan, kelembagaan FROM aspek_manajemen_dan_organisasi WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $polaManajemen = $row['pola_manajemen'];
              $sistemPengelolaan = $row['sistem_pengelolaan'];
              $Kelembagaan = $row['kelembagaan'];
            }
          }else {
            $polaManajemen = '';
            $sistemPengelolaan = '';
            $Kelembagaan = '';
        }
        ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="manajemen">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Aspek Manajemen dan Organisasi</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin6.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                     <div class="form-group">
                        <label for="PolaManajemen">Pola Manajemen (apakah swasta murni atau kemitraan) :</label>
                        <textarea class="form-control" name="polamanajemen" rows="5" cols="100" required><?php echo $polaManajemen ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="SisPengelolaan">Sistem pengelolaan (Struktur organisasi dari pihak-pihak yang terlibat di investasi tersebut dan wewenang serta tanggung jawabnya) :</label>
                        <textarea class="form-control" name="sispengelolaan" rows="5" cols="100" required><?php echo $sistemPengelolaan ?></textarea>
                      </div>
                       <div class="form-group">
                        <label for="Kelembagaan">Kelembagaan (apakah ada instansi lain yang terkait, sebagai contoh: perhutani) :</label>
                        <textarea class="form-control" name="kelembagaan" rows="5" cols="100" required><?php echo $Kelembagaan ?></textarea>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save"/>
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">

        <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT identifikasi_awal_dampak_lingkungan, rencana_pengadaan_tanah, identifikasi_awal_dampak_sosial, sumber_daya_alam, sumber_daya_manusia FROM aspek_sosial_dan_lingkungan WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $identifikasiLingkungan = $row['identifikasi_awal_dampak_lingkungan'];
              $rencanaTanah = $row['rencana_pengadaan_tanah'];
              $identifikasiSosial = $row['identifikasi_awal_dampak_sosial'];
              $sda = $row['sumber_daya_alam'];
              $sdm = $row['sumber_daya_manusia'];
            }
          }else {
              $identifikasiLingkungan = '';
              $rencanaTanah = '';
              $identifikasiSosial = '';
              $sda = '';
              $sdm = '';
        }
        ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="sosial">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Aspek Sosial dan Lingkungan</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin7.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                    <div class="form-group">
                        <label for="IdentifikasiLingkungan">Identifikasi Awal Dampak Lingkungan :</label>
                        <textarea class="form-control" rows="5" cols="100" name="identifikasilingkungan" required><?php echo $identifikasiLingkungan ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="RencanaTanah">Rencana Pengadaan Tanah :</label>
                        <textarea class="form-control" name="rencanatanah" rows="5" cols="100" required><?php echo $rencanaTanah ?></textarea>
                      </div>
                       <div class="form-group">
                        <label for="IdentifikasiSosial">Identifikasi Awal Dampak Sosial :</label>
                        <textarea class="form-control" name="identifikasisosial" rows="5" cols="100" required><?php echo $identifikasiSosial ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="SDA">Sumber Daya Alam(potensi bahan baku yang tersedia) :</label>
                        <textarea class="form-control" name="sda" rows="5" cols="100" required><?php echo $sda ?></textarea>
                      </div>
                      <div class="form-group">
                        <label for="SDM">Sumber daya manusia (jumlah tenaga kerja yang dibutuhkan dan potensi tenaga pekerja disekitar lokasi yang dapat diserap oleh proyek tersebut) :</label>
                        <textarea class="form-control" name="sdm" rows="5" cols="100" required><?php echo $sdm ?></textarea>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">

        <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT analisis_alternatif_pembiayaan_bisnis, penetapan_faktor_diskonto, asumsi_penyusunan_model_keuangan, asumsi_belanja_modal, asumsi_biaya_operasional_pemeliharaan, analisis_pendapatan_bisnis, proyeksi_kelayakan_keuangan, analisis_sensivitas FROM aspek_keuangan WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $analisisBisnis = $row['analisis_alternatif_pembiayaan_bisnis'];
              $penetapanDiskonto = $row['penetapan_faktor_diskonto'];
              $asumsiModelkeuangan = $row['asumsi_penyusunan_model_keuangan'];
              $asumsiBelanjamodal = $row['asumsi_belanja_modal'];
              $asumsiBiayaoperasional = $row['asumsi_biaya_operasional_pemeliharaan'];
              $analisisPendapatanbisnis = $row['analisis_pendapatan_bisnis'];
              $proyeksiKeuangan = $row['proyeksi_kelayakan_keuangan'];
              $analisisSensivitas = $row['analisis_sensivitas'];
            }
          }else {
              $analisisBisnis = '';
              $penetapanDiskonto = '';
              $asumsiModelkeuangan = '';
              $asumsiBelanjamodal = '';
              $asumsiBiayaoperasional = '';
              $analisisPendapatanbisnis = '';
              $proyeksiKeuangan = '';
              $analisisSensivitas = '';
        }
        ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="keuangan">
          <div class="my-auto">
            <h1 class="mb-5"></h1><br/>
            <h2 class="mb-5">Aspek Keuangan</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin8.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                      <div class="form-group">
                          <label for="AnalisisBisnis">Analisis Alternatif Pembiayaan Bisnis :</label>
                          <textarea class="form-control" rows="5" cols="100" name="analisisbisnis" required><?php echo $analisisBisnis ?></textarea>
                      </div>
                      <div class="form-group">
                          <label for="AnalisisKeuangan">Analisis Keuangan :</label><br/>
                          <label for="FaktorDiskonto">a. Penetapan Faktor Diskonto :</label>
                          <textarea class="form-control" name="faktordiskonto" rows="5" cols="100" required><?php echo $penetapanDiskonto ?></textarea>
                          <label for="AsumsiKeuangan">b. Asumsi Dasar Penyusunan Model Keuangan (Asumsi Pertumbuhan Pendapatan, Biaya dan Lain-lain) :</label>
                          <textarea class="form-control" name="asumsikeuangan" rows="5" cols="100" required><?php echo $asumsiModelkeuangan ?></textarea>
                          <label for="AsumsiModal">c. Asumsi Belanja Modal (untuk Investasi Bangunan, Mesin, dll) :</label>
                          <textarea class="form-control" name="asumsimodal" rows="5" cols="100" required><?php echo $asumsiBelanjamodal ?></textarea>
                          <label for="AsumsiOperasional">d. Asumsi Biaya Operasional dan Pemeliharaan :</label>
                          <textarea class="form-control" name="asumsioperasional" rows="5" cols="100" required><?php echo $asumsiBiayaoperasional ?></textarea>
                      </div>
                      <div class="form-group">
                          <label for="AnalisisPendapatan">Analisis Pendapatan Bisnis :</label>
                          <textarea class="form-control" name="analisispendapatan" rows="5" cols="100" required><?php echo $analisisPendapatanbisnis ?></textarea>
                      </div>
                      <div class="form-group">
                          <label for="HasilProyeksi">Hasil Proyeksi Keuangan dan Analisis Kelayakan Keuangan(NPV,IRR, dan Payback Period) :</label>
                          <textarea class="form-control" name="hasilproyeksi" rows="5" cols="100" required><?php echo $proyeksiKeuangan ?></textarea>
                      </div>
                      <div class="form-group">
                          <label for="AnalisisSensivitas">Analisis Sensivitas :</label>
                          <textarea class="form-control" name="analisissensivitas" rows="5" cols="100" required><?php echo $analisisSensivitas ?></textarea>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <hr class="m-0">

         <?php
          $idDes = $_GET['id_deskripsi_proyek'];
          $query="SELECT nama_instansi, alamat, no_telp, email FROM nara_hubung WHERE id_deskripsi_proyek='$idDes'";
          $result = $conn-> query($query);
          if ($result->num_rows > 0){
            while ($row = $result-> fetch_assoc()){
              $NamaInstansi = $row['nama_instansi'];
              $Alamat = $row['alamat'];
              $noTelp = $row['no_telp'];
              $Email = $row['email'];
            }
          }else {
              $NamaInstansi = '';
              $Alamat = '';
              $noTelp = '';
              $Email = '';
        }
        ?>

        <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="narahubung">
          <div class="my-auto">
            <h2 class="mb-5">Narahubung</h2>
              <div class="container">
                 <div class="row">
                    <div class="col-lg-8 mx-auto">
                      <div class="modal-body">
                    <!-- Project Details Go Here -->
                    <form method="post" action="php/saveadmin9.php?id_deskripsi_proyek=<?php echo($idDes)?>">
                      <div class="form-group">
                        <label for="NamaInstansi">Nama :</label>
                        <input type="text" class="form-control" name="nama_instansi" value="<?php echo $NamaInstansi ?>"required><br/>
                      </div>
                      <div class="form-group">
                        <label for="AlamatInstansi">Alamat :</label>
                        <input type="text" class="form-control" name="alamat" value="<?php echo $Alamat ?>" required><br/>
                      </div>
                      <div class="form-group">
                        <label for="NoTelp">No Telepon :</label>
                        <input type="text" class="form-control" name="notelp" value="<?php echo $noTelp ?>" required><br/>
                      </div>
                      <div class="form-group">
                        <label for="Email">Email :</label>
                        <input type="email" class="form-control" name="email" value="<?php echo $Email ?>" required><br/>
                      </div>
                      <input type="submit" name="save" class="btn btn-info" value="Save"/>
                    </form><br/>
                    <form method="post" action="php/exportToWordFromDatabaseAdmin.php" enctype="multipart/form-data">
                      <input type="submit" name="create_word" class="btn btn-info" value="Export to Word" />
                    </form><br/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>



    <!-- Footer -->
    <footer>
      <div class="container-fluid">
       <div class="p-3 mb-2 bg-dark text-white">
          <div class="row">
            <div class="col-md-4">
              <ul class="list-inline-quicklinks">
              </ul>
            </div>
            <div class="col-md-4">
              <ul class="list-inline quicklinks">
                <li class="list-inline-item">
                  <span class="copyright">Copyright © 2019 Keris Central Java Incorporated. All Rights Reserved</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>


  </body>

</html>



